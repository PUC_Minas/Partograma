    package com.projetos.medicina.partograma;
    /**
     * @author Paulo Victor de Oliveira Leal
     */

    // Dependencias
    import android.app.Activity;
    import android.content.Intent;
    import android.content.SharedPreferences;
    import android.graphics.Color;
    import android.graphics.PorterDuff;
    import android.os.Bundle;
    import android.view.Menu;
    import android.view.MenuItem;
    import android.view.View;
    import android.widget.Button;
    import android.widget.CheckBox;
    import android.widget.ImageButton;
    import android.widget.Toast;

    /**
     * Classe que constroi o menu de opcoes
     */
    public class Options extends Activity {
        // Variaveis globais
        int resp = 0;
        // Cria o ImageButton
        ImageButton button1;
        ImageButton button2;
        ImageButton button3;
        ImageButton button4;
        ImageButton button5;
        ImageButton button6;
        ImageButton button7;
        ImageButton button8;
        ImageButton button9;
        ImageButton button11;
        ImageButton button12;
        ImageButton button13;
        ImageButton button15;
        ImageButton button14;
        ImageButton button131;

        // Cria o Buttn
        Button button;
        // Cria o SharedPreferences
        SharedPreferences sp = null;
        // Cria os CheckBox
        public CheckBox first, second, dil;
        // Cria a intent
        Intent result = new Intent();

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_options);
            // Inicia o SharedPreferences
            sp = getSharedPreferences("partograma",Activity.MODE_PRIVATE);
            // Inicia os CheckBox
            first     = (CheckBox) findViewById(R.id.checkBox1);
            second    = (CheckBox) findViewById(R.id.checkBox2);
            dil = (CheckBox) findViewById(R.id.checkBox3);

            button1 = (ImageButton) findViewById(R.id.imageButton1);
            button2 = (ImageButton) findViewById(R.id.imageButton2);
            button3 = (ImageButton) findViewById(R.id.imageButton3);
            button4 = (ImageButton) findViewById(R.id.imageButton4);
            button5 = (ImageButton) findViewById(R.id.imageButton5);
            button6 = (ImageButton) findViewById(R.id.imageButton6);
            button7 = (ImageButton) findViewById(R.id.imageButton7);
            button8 = (ImageButton) findViewById(R.id.imageButton8);
            button9 = (ImageButton) findViewById(R.id.imageButton9);
            button11 = (ImageButton) findViewById(R.id.imageButton11);
            button12 = (ImageButton) findViewById(R.id.imageButton12);
            button13 = (ImageButton) findViewById(R.id.imageButton13);
            button15 = (ImageButton) findViewById(R.id.imageButton15);
            button14 = (ImageButton) findViewById(R.id.imageButton14);
            button131 = (ImageButton) findViewById(R.id.imageButton131);

            button1.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
            button2.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
            button3.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
            button4.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
            button5.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
            button6.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
            button7.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
            button8.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
            button9.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
            button11.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
            button12.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
            button13.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
            button15.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
            button14.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
            button131.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);

            // Chamada dos botoes de Posicao da Cabeca
            buttonClick01();
            buttonClick02();
            buttonClick03();
            buttonClick04();
            buttonClick05();
            buttonClick06();
            buttonClick07();
            buttonClick08();
            buttonClick09();
            // Chamada dos botoes de Contracao
            buttonClick11();
            buttonClick12();
            buttonClick13();
            // Chamada do botao para limpar as opcoes ativas
            buttonClickLimpar();
            // Chamada do botao para finalizar as Opcoes
            buttonClickFinish();
        }

        /**
         *  Cria o click do botao de opcao de cabeca
         */
        public void buttonClick01() {
            button1 = (ImageButton) findViewById(R.id.imageButton1);
            button1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    if( sp.getInt("position", -1) >= 100) {
                        resp = 1;
                        Toast.makeText(getApplicationContext(), "Posicao selecionada", Toast.LENGTH_SHORT).show();
                        sp.edit().putInt("cab", resp).commit();
                        sp.edit().putBoolean("teste", true);
                        result.putExtra("cab", resp);
                        setResult(RESULT_OK, result);
                        button1.getBackground().setColorFilter(Color.YELLOW, PorterDuff.Mode.MULTIPLY);
                        button2.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button3.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button4.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button5.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button6.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button7.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button8.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button9.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                    } else {
                        Toast.makeText(getApplicationContext(), "Nao é possivel colocar nessa posicao!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
        /**
         *  Cria o click do botao de opcao de cabeca
         */
        public void buttonClick02() {
            button2 = (ImageButton) findViewById(R.id.imageButton2);
            button2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    if( sp.getInt("position", -1) >= 100) {
                        resp = 2;
                        Toast.makeText(getApplicationContext(), "Posicao selecionada", Toast.LENGTH_SHORT).show();
                        sp.edit().putInt("cab", resp).commit();
                        sp.edit().putBoolean("teste", true);
                        result.putExtra("cab", resp);
                        setResult(RESULT_OK, result);
                        button1.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button2.getBackground().setColorFilter(Color.YELLOW, PorterDuff.Mode.MULTIPLY);
                        button3.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button4.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button5.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button6.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button7.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button8.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button9.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                    }else{
                        Toast.makeText(getApplicationContext(), "Nao é possivel colocar nessa posicao!", Toast.LENGTH_SHORT).show();
                    }

                }

            });
        }

        /**
         *  Cria o click do botao de opcao de cabeca
         */
        public void buttonClick03() {
            button3 = (ImageButton) findViewById(R.id.imageButton3);
            button3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    if( sp.getInt("position", -1) >= 100) {
                        resp = 3;
                        Toast.makeText(getApplicationContext(), "Posicao selecionada", Toast.LENGTH_SHORT).show();
                        sp.edit().putInt("cab", resp).commit();
                        sp.edit().putBoolean("teste", true);
                        result.putExtra("cab", resp);
                        setResult(RESULT_OK, result);
                        button1.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button2.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button3.getBackground().setColorFilter(Color.YELLOW, PorterDuff.Mode.MULTIPLY);
                        button4.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button5.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button6.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button7.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button8.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button9.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                    } else {
                        Toast.makeText(getApplicationContext(), "Nao é possivel colocar nessa posicao!", Toast.LENGTH_SHORT).show();
                    }
                }

            });
        }

        /**
         *  Cria o click do botao de opcao de cabeca
         */
        public void buttonClick04() {
            button4 = (ImageButton) findViewById(R.id.imageButton4);
            button4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    if( sp.getInt("position", -1) >= 100) {
                        resp = 4;
                        Toast.makeText(getApplicationContext(), "Posicao selecionada", Toast.LENGTH_SHORT).show();
                        sp.edit().putInt("cab", resp).commit();
                        sp.edit().putBoolean("teste", true);
                        result.putExtra("cab", resp);
                        setResult(RESULT_OK, result);
                        button1.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button2.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button3.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button4.getBackground().setColorFilter(Color.YELLOW, PorterDuff.Mode.MULTIPLY);
                        button5.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button6.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button7.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button8.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button9.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                    } else {
                        Toast.makeText(getApplicationContext(), "Nao é possivel colocar nessa posicao!", Toast.LENGTH_SHORT).show();
                    }
                }

            });
        }

        /**
         *  Cria o click do botao de opcao de cabeca
         */
        public void buttonClick05() {
            button5 = (ImageButton) findViewById(R.id.imageButton5);
            button5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    if( sp.getInt("position", -1) >= 100) {
                        resp = 5;
                        Toast.makeText(getApplicationContext(), "Posicao selecionada", Toast.LENGTH_SHORT).show();
                        sp.edit().putInt("cab", resp).commit();
                        sp.edit().putBoolean("teste", true);
                        result.putExtra("cab", resp);
                        setResult(RESULT_OK, result);
                        button1.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button2.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button3.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button4.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button5.getBackground().setColorFilter(Color.YELLOW, PorterDuff.Mode.MULTIPLY);
                        button6.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button7.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button8.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button9.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                    } else {
                        Toast.makeText(getApplicationContext(), "Nao é possivel colocar nessa posicao!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

        /**
         *  Cria o click do botao de opcao de cabeca
         */
        public void buttonClick06() {
            button6 = (ImageButton) findViewById(R.id.imageButton6);
            button6.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    if( sp.getInt("position", -1) >= 100) {
                        resp = 6;
                        Toast.makeText(getApplicationContext(), "Posicao selecionada", Toast.LENGTH_SHORT).show();
                        sp.edit().putInt("cab", resp).commit();
                        sp.edit().putBoolean("teste", true);
                        result.putExtra("cab", resp);
                        setResult(RESULT_OK, result);
                        button1.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button2.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button3.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button4.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button5.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button6.getBackground().setColorFilter(Color.YELLOW, PorterDuff.Mode.MULTIPLY);
                        button7.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button8.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button9.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                    } else {
                        Toast.makeText(getApplicationContext(), "Nao é possivel colocar nessa posicao!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

        /**
         *  Cria o click do botao de opcao de cabeca
         */
        public void buttonClick07() {
            button7 = (ImageButton) findViewById(R.id.imageButton7);
            button7.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    if( sp.getInt("position", -1) >= 100) {
                        resp = 7;
                        Toast.makeText(getApplicationContext(), "Posicao selecionada", Toast.LENGTH_SHORT).show();
                        sp.edit().putInt("cab", resp).commit();
                        sp.edit().putBoolean("teste", true);
                        result.putExtra("cab", resp);
                        setResult(RESULT_OK, result);
                        button1.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button2.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button3.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button4.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button5.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button6.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button7.getBackground().setColorFilter(Color.YELLOW, PorterDuff.Mode.MULTIPLY);
                        button8.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button9.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                    } else {
                        Toast.makeText(getApplicationContext(), "Nao é possivel colocar nessa posicao!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

        /**
         *  Cria o click do botao de opcao de cabeca
         */
        public void buttonClick08() {
            button8 = (ImageButton) findViewById(R.id.imageButton8);
            button8.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    if( sp.getInt("position", -1) >= 100) {
                        resp = 8;
                        Toast.makeText(getApplicationContext(), "Posicao selecionada", Toast.LENGTH_SHORT).show();
                        sp.edit().putInt("cab", resp).commit();
                        sp.edit().putBoolean("teste", true);
                        result.putExtra("cab", resp);
                        setResult(RESULT_OK, result);
                        button1.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button2.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button3.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button4.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button5.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button6.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button7.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button8.getBackground().setColorFilter(Color.YELLOW, PorterDuff.Mode.MULTIPLY);
                        button9.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                    } else {
                        Toast.makeText(getApplicationContext(), "Nao é possivel colocar nessa posicao!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

        /**
         *  Cria o click do botao de opcao de cabeca
         */
        public void buttonClick09() {
            button9 = (ImageButton) findViewById(R.id.imageButton9);
            button9.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    if( sp.getInt("position", -1) >= 100) {
                        resp = 9;
                        Toast.makeText(getApplicationContext(), "Posicao selecionada", Toast.LENGTH_SHORT).show();
                        sp.edit().putInt("cab", resp).commit();
                        sp.edit().putBoolean("teste", true);
                        result.putExtra("cab", resp);
                        setResult(RESULT_OK, result);
                        button1.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button2.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button3.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button4.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button5.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button6.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button7.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button8.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button9.getBackground().setColorFilter(Color.YELLOW, PorterDuff.Mode.MULTIPLY);
                    } else {
                        Toast.makeText(getApplicationContext(), "Nao é possivel colocar nessa posicao!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

        /**
         * Cria o click do botao de opcao de Contracao
         */
        public void buttonClick11() {
            button11 = (ImageButton) findViewById(R.id.imageButton11);
            button11.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    if( sp.getInt("position", -1) >= 100) {
                        Toast.makeText(getApplicationContext(), "Contracao selecionada", Toast.LENGTH_SHORT).show();
                        sp.edit().putInt("cont", 1).commit();
                        button11.getBackground().setColorFilter(Color.YELLOW, PorterDuff.Mode.MULTIPLY);
                        button12.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button13.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                    } else {
                        Toast.makeText(getApplicationContext(), "Nao é possivel colocar nessa posicao!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

        /**
         * Cria o click do botao de opcao de Contracao
         */
        public void buttonClick12() {
            button12 = (ImageButton) findViewById(R.id.imageButton12);
            button12.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    if( sp.getInt("position", -1) >= 100) {
                        Toast.makeText(getApplicationContext(), "Contracao selecionada", Toast.LENGTH_SHORT).show();
                        sp.edit().putInt("cont", 2).commit();
                        button11.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button12.getBackground().setColorFilter(Color.YELLOW, PorterDuff.Mode.MULTIPLY);
                        button13.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                    } else {
                        Toast.makeText(getApplicationContext(), "Nao é possivel colocar nessa posicao!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

        /**
         * Cria o click do botao de opcao de Contracao
         */
        public void buttonClick13() {
            button13 = (ImageButton) findViewById(R.id.imageButton13);
            button13.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    if( sp.getInt("position", -1) >= 100) {
                        Toast.makeText(getApplicationContext(), "Contracao selecionada", Toast.LENGTH_SHORT).show();
                        sp.edit().putInt("cont", 3).commit();
                        button11.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button12.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                        button13.getBackground().setColorFilter(Color.YELLOW, PorterDuff.Mode.MULTIPLY);
                    } else {
                        Toast.makeText(getApplicationContext(), "Nao é possivel colocar nessa posicao!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

        /**
         * Finaliza a classe opcoes
         * Testa todos os CheckBox e insere no SharedPrefences
         */
        public void buttonClickFinish() {
            button = (Button) findViewById(R.id.button);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {

                    if (first.isChecked()){
                        sp.edit().putBoolean("first", true).commit();
                    }
                   // if (second.isChecked()){
                   //     sp.edit().putBoolean("second", true).commit();
                   // }
                    if (dil.isChecked() && sp.getInt("position", -1) <= 99){
                        sp.edit().putBoolean("dila", true).commit();
                    } else if (dil.isChecked() && sp.getInt("position", -1) > 99) {
                        Toast.makeText(getApplicationContext(), "Nao e possivel inserir dilatacao nessa posicao", Toast.LENGTH_SHORT).show( );
                    }
                    sp.edit().putBoolean("opend", true).commit();
                    Toast.makeText(getApplicationContext(),"Pronto", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(Options.this,MainActivity.class);

                    // Fala para a nova Activity inicar, limpando a pilha do Java
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.putExtra("options",true);
                    startActivity(i);
                }

            });
        }

        /**
         * Botao para limpar todos os parametros da posicao da grade
         */
        public void buttonClickLimpar() {
            button = (Button) findViewById(R.id.button2);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    sp.edit().putBoolean("first",  false).commit();
                    sp.edit().putBoolean("second", false).commit();
                    sp.edit().putBoolean("dila",    false).commit();
                    sp.edit().putInt("cont",  0).commit();
                    sp.edit().putInt("cab",        0).commit();
                    Toast.makeText(getApplicationContext(), "Limpando...", Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.menu_options, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.
            int id = item.getItemId();

            //noinspection SimplifiableIfStatement
            if (id == R.id.action_settings) {
                return true;
            }

            return super.onOptionsItemSelected(item);
        }
    }
