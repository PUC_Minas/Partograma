package com.projetos.medicina.partograma;
/**
 *
 * @author  Paulo Victor de Oliveira Leal
 * @version 0.2
 *
 * Objetivo do projeto desenvolver a versao mobile do Partograma
 *
 * O MainActivity cria a grade inserindo em cada posicao um adaptador da classe CustomAdapter.
 * Esse adaptador e composto de cinco ImageView e um TextView. A classe CustomAdapter chama
 * a Options para selecionar para essa posicao da grade o que ela deve mostrar. A classe Options
 * nove ImageButton para escolher a posicao da cabeca, tres ImageButton para escolher a contracao,
 * e tres CheckBox para escolher o restante. Escolhido dentre as opcoes, a classe chama novamente
 * o MainActivity limpando a pilha do java e passando os parametros da posicao da grade. Apartir
 * dai o processo se repete.
 *
 * Atualizacao 01 - Mudanca de inicializacao de tabelas
 *                  Objeto Tabelas criado
 *                  Correcao de escrita e carregamento do banco de dados.
 *                  Correcao no layout retrato.
 *                  Novo botao para salvar.
 *                  Remocao de um checkbox de batimento cardiaco
 *                  Troca de posicao da dilatacao (>22 && <119).
 *                  Troca de posicao contracao (<120 && >230)
 *                  Troca de posicao cabeca (<120 && >230)
 *                  Troca do cabecalho para hora
 *                  Insercao e carregamento do banco de dados com nome maisculo ou minusculo
 *
 * Atualizacao 02 -
 *
 */

// Dependecias
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Classe que constroi a grade e inicia as posicoes
 * @version 1.1
 */
public class MainActivity extends Activity {
    // Variaveis globais
    GridView gv;

    String[] tvDados = Tabelas.tvDados;
    String[] eDados = Tabelas.eDados;
    int[] cabP = Tabelas.cabP;
    int[] contP = Tabelas.contP;
    int[] bcf1P = Tabelas.bcf1P;
    int[] dilP = Tabelas.dilP;
    int[] bcf2P = Tabelas.bcf2P;

    // Cria o banco de dados SQLite
    DataBase db;
    // Cria o botao
    Button button;
    int posic = 0;
    CustomAdapter customAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activitymain);
        // Inicia o banco de dados
        db = new DataBase(getApplicationContext());
        // Carrega todas as posicoes inseridas no banco de dados
        SharedPreferences ssp = getSharedPreferences("login", MODE_PRIVATE);
        // Variaveis Locais
        Intent intent = getIntent();
        String nome;
        // Teste se e a primeira execucao do programa para o mesmo usuario
        if(ssp.getBoolean("primeiraExec", false)){
            //limparArrays();
            String hora = getHoraTime();
            String min = getMinTime();
            String hAlt;
            // Pega o tempo
            for(int i = 3; i<20; i++){
                hAlt = ""+(Integer.parseInt(hora)+(i-3));
                    hAlt = ""+(Integer.parseInt(hAlt)%24);
                    if(hAlt.length() < 2){
                        tvDados[i] = " 0" + hAlt + ":" + min;
                    }else {
                        tvDados[i] = " " + hAlt + ":" + min;
                    }
            }
            System.out.println("<> Hora ["+hora+"hr"+"]");
        }
        ssp.edit().putBoolean("primeiraExec", false).commit();
        nome = intent.getStringExtra("login");

        boolean status = intent.getBooleanExtra("status", false);
        SharedPreferences  sharedPreferences = getSharedPreferences("config", MODE_PRIVATE);
        gv=(GridView) findViewById(R.id.gridView1);
        // Fica tentando carregar o nome enquanto for igual a null
        if (nome != null) {
            sharedPreferences.edit().putString("name", nome).commit();
            System.out.println("<> Nome [" + nome+"]");

            if (tagSearch(nome) && status) {
                System.out.println("<> ["+nome+"] Vai ser carregado");
                gv.setAdapter(customAdapter = carregarDoBD(nome));

            }else {
                gv.setAdapter(criarGradeAtual());
            }
            // Recarrega o nome
            nome = sharedPreferences.getString("name", null);
            status = false;

        } else {
            nome = intent.getStringExtra("login");
        }

        nome = sharedPreferences.getString("name", null);


        // Ativa os botoes de salvar e limpar
        buttonSalvar(nome);
        buttonLimpar();

        // Carrega varivaies entre as Activity
        SharedPreferences sp = getSharedPreferences("partograma", MODE_PRIVATE);
        customAdapter = new CustomAdapter(this, tvDados, cabP, bcf1P, bcf2P, dilP, contP);

        // Recebe da classe Options os dados selecionados
        int cab = sp.getInt("cab", -1);
        int pos = sp.getInt("position", -1);
        // Tratamento para cada tipo de Contracao
        int cont = sp.getInt("cont", -1);

        // Tratamento se a posicao for negativa (ela vai ser negativa somente na primeira execucao)
        if(sp.getInt("position", -1) < 0){
            pos = 0;
        }

        // Monta a posicao da grade com os valores recebidos
        inserirCab(cab, pos);
        inserirContracao(cont, pos);
        inserirBatimento01(sp.getBoolean("first", false), pos);
        inserirBatimento02(sp.getBoolean("second", false), pos);
        inserirDilatacao(sp.getBoolean("dila", false), pos);
        customAdapter.notifyDataSetChanged();

        // Limpa as variaveis no SharedPreferences
        sp.edit().putInt("position", -1).commit();
        sp.edit().putInt("cab", -1).commit();
        sp.edit().putInt("cont", -1).commit();
        sp.edit().putBoolean("first", false).commit();
        sp.edit().putBoolean("second", false).commit();
        sp.edit().putBoolean("dila", false).commit();

        // Salva a posicao
        posic = pos;

        // Constroi a grade
        gv.setAdapter(customAdapter);

        // Cria a segunda grade
        GridView grid = (GridView) findViewById(R.id.grid2);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, eDados);

        // Constroi a segunda grade
        grid.setAdapter(adapter);
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                Toast.makeText(getApplicationContext(),
                        ((TextView) v).getText(), Toast.LENGTH_SHORT).show();
            }
        });

        // Fecha o banco de dados
        db.closeDB();
    }

    /**
     * Monta a grade para valores recebidos das opcoes
     *
     * @since Atualizacao 01
     * @return customAdapter - Adaptador com os valores para a grade
     */
    public CustomAdapter criarGradeAtual( ){
        // Carrega o SharedPreferences
        SharedPreferences sp = getSharedPreferences("partograma", MODE_PRIVATE);
        // Recebe da classe Options os dados selecionados
        int cab = sp.getInt("cab", 0);
        int pos = sp.getInt("position", 0);
        // Tratamento se a posicao for negativa (ela vai ser negativa somente na primeira execucao)
        if(sp.getInt("position", 0) < 0){
            pos = 0;
        }

        // Insere na posicao atual os valores recebidos
        inserirCab(cab, pos);
        int cont = sp.getInt("cont", 0);
        inserirContracao(cont, pos);
        boolean first = sp.getBoolean("first", false);
        inserirBatimento01(first, pos);
        boolean second = sp.getBoolean("second", false);
        inserirBatimento02(second, pos);
        boolean dila = sp.getBoolean("dila", false);
        inserirDilatacao(dila, pos);

        // Monta o retorno
        customAdapter = new CustomAdapter(this, tvDados, cabP, bcf1P, bcf2P, dilP, contP);
        customAdapter.notifyDataSetChanged();
        return  customAdapter;
    }

    /**
     * Cria o botao salvar
     *
     * @since Atualizacao 01
     * @param nome - Nome do usuario para criar a grade
     */
    public void buttonSalvar( final String nome) {
        button = (Button) findViewById(R.id.salvar);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                inserirNoBD(nome);
                Toast.makeText(getApplicationContext(), "Salvo com sucesso!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Cria o botao de limpar a grade
     *
     * @since Atualizacao 01
     */
    public void buttonLimpar( ) {
        button = (Button) findViewById(R.id.limpar);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                limparArrays();
                Intent intent = new Intent(MainActivity.this, MainActivity.class);
                // Ao chamar a activity por cima garantir que vai limpar a fila
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
    }

    /**
     * Metodo para inserir a posicao da cabeca
     *
     * @since Atualizacao 01
     * @param cab - Posicao da cabeca
     * @param pos - Posicao da grade
     * @return pos - Retorna a posicao inserida
     */
    public int inserirCab( int cab, int pos ){
        // Tratamento para cada tipo de posicao da cabeca
        if(cab == 0){
            cabP[pos] = -1;
        } else if(cab == 1){
            cabP[pos] = R.drawable.cab1;

        } else if(cab == 2){
            cabP[pos] = R.drawable.cab2;

        } else if(cab == 3){
            cabP[pos] = R.drawable.cab3;

        } else if(cab == 4){
            cabP[pos] = R.drawable.cab4;

        } else if(cab == 5){
            cabP[pos] = R.drawable.cab5;

        } else if(cab == 6){
            cabP[pos] = R.drawable.cab6;

        } else if(cab == 7){
            cabP[pos] = R.drawable.cab7;

        } else if(cab == 8){
            cabP[pos] = R.drawable.cab8;

        } else if(cab == 9){
            cabP[pos] = R.drawable.cab9;

        }
        return pos;
    }

    /**
     * Metodo para pegar a hora do sistema
     *
     * @since Atualizacao 01
     * @return data
     */
    public String getHoraTime() {
        DateFormat dateFormat = new SimpleDateFormat("HH");
        Date date = new Date();
        return dateFormat.format(date);
    }

    /**
     * Metodo para pegar os minutos do sistema
     *
     * @since Atualizacao 01
     * @return data
     */
    public String getMinTime() {
        DateFormat dateFormat = new SimpleDateFormat("mm");
        Date date = new Date();
        return dateFormat.format(date);
    }

    /**
     * Metodo para inserir a contracao
     *
     * @since Atualizacao 01
     * @param cont - Contracao
     * @param pos - Posicao da grade
     * @return pos - Posicao inserida
     */
    public int inserirContracao( int cont, int pos){
        // Tratamento para cada tipo de Contracao

        if(cont == 0){
            contP[pos] = -1;

        } else if(cont == 1){
            contP[pos] = R.drawable.cont1;

        }else if (cont == 2){
            contP[pos] = R.drawable.cont2;

        }else if (cont == 3){
            contP[pos] = R.drawable.cont3;

        }
        return pos;
    }

    /**
     * Metodo para inserir o batimento cardiaco
     *
     * @since Atualizacao 01
     * @param first - Batimento cardiaco
     * @param pos - Posicao da grade
     * @return pos - Posicao inserida
     */
    public int inserirBatimento01( boolean first, int pos ){
        if(first){
            bcf1P[pos] = R.drawable.bcf1;
        } else{
            bcf1P[pos] = -1;
        }
        return pos;
    }

    /**
     * Metodo para inserir o batimento cardiaco 2
     *
     * @since Atualizacao 01
     * @param second - Batimento cardiaco
     * @param pos - Posicao da grade
     * @return pos - Posicao inserida
     */
    public int inserirBatimento02( boolean second, int pos ){
        if(second){
            bcf2P[pos] = R.drawable.bcf2;
        } else{
            bcf2P[pos] = -1;
        }
        return pos;
    }

    /**
     * Metodo para inserir a dilatacao
     *
     * @since Atualizacao 01
     * @param dila - Dilatacao
     * @param pos - Posicao da grade
     * @return pos - Posicao inserida
     */
    public int inserirDilatacao( boolean dila, int pos ){
        if(dila){
            dilP[pos] = R.drawable.dil;
        } else{
            dilP[pos] = -1;
        }
        return pos;
    }

    /**
     * Carregar dados do banco de dados
     *
     * @since Atualizacao 01
     * @param nome - Nome para pesquisar TAG
     * @return customAdapter - Adaptador da grade
     */
    public CustomAdapter carregarDoBD( String nome ){
        List<Dados> tagDados = db.getAllToDosByTag(nome.toLowerCase());
        System.out.println( "<> Tag["+nome+"] - Tam["+tagDados.size()+"]");
        int o = 0;
        for (Dados data : tagDados){
            String[] arrayConvertido = data.getNote().split(",");
            for(int i = 0; i<240; i++) {
                if(o == 1) {
                    cabP[i] = Integer.parseInt((arrayConvertido[i].replaceAll("[\\D]", "")));
                }else if(o == 2 ){
                    bcf1P[i] = Integer.parseInt((arrayConvertido[i].replaceAll("[\\D]", "")));
                }else if(o == 3 ){
                    bcf2P[i] = Integer.parseInt((arrayConvertido[i].replaceAll("[\\D]", "")));
                }else if(o == 4 ){
                    dilP[i] = Integer.parseInt((arrayConvertido[i].replaceAll("[\\D]", "")));
                }else if(o == 5 ){
                    contP[i] = Integer.parseInt((arrayConvertido[i].replaceAll("[\\D]", "")));
                }else if(o == 0 ) {
                    if(arrayConvertido[i].replaceAll("[\\D]", "").length() >3) {
                        tvDados[i] = "  " + (arrayConvertido[i].replaceAll("[\\D]", "").charAt(0) + ""  +
                                             arrayConvertido[i].replaceAll("[\\D]", "").charAt(1) + ":" +
                                             arrayConvertido[i].replaceAll("[\\D]", "").charAt(2) + ""  +
                                             arrayConvertido[i].replaceAll("[\\D]", "").charAt(3));
                    }else{
                        tvDados[i] = "  " + (arrayConvertido[i].replaceAll("[\\D]", ""));
                    }
                }
            }
            o++;
        }
        o = 0;
        customAdapter = new CustomAdapter(this, tvDados, cabP, bcf1P, bcf2P, dilP, contP);
        customAdapter.notifyDataSetChanged();
        return  customAdapter;
    }

    /**
     * Insere no banco de dados
     *
     * @since Atualizacao 01
     * @param nome - Nome da TAG a ser criada
     */
    public void inserirNoBD( String nome ){
        // Se ja existir dados para esse nome deletar dados antigos
        if(tagSearch(nome)){
            db.deleteTag(tagByName(nome), true);
        }

        // Cria a TAG novamente
        Tag usuario = new Tag(nome.toLowerCase());
        long idTag = db.createTag(usuario);

        // Recebe os dados
        Dados textData     = new Dados(Arrays.toString(tvDados), 0);
        Dados posCabeca    = new Dados(Arrays.toString(cabP)   , 0);
        Dados posBcf1      = new Dados(Arrays.toString(bcf1P)  , 0);
        Dados posBcf2      = new Dados(Arrays.toString(bcf2P)  , 0);
        Dados posDilatacao = new Dados(Arrays.toString(dilP)   , 0);
        Dados posContracao = new Dados(Arrays.toString(contP)  , 0);

        // Insere os dados
        db.createToDo(textData,     new long[]{idTag});
        db.createToDo(posCabeca,    new long[]{idTag});
        db.createToDo(posBcf1,      new long[]{idTag});
        db.createToDo(posBcf2,      new long[]{idTag});
        db.createToDo(posDilatacao, new long[]{idTag});
        db.createToDo(posContracao, new long[]{idTag});

        System.out.print("<> Insercao no BD ");
        System.out.println(tagSearch(nome));
    }

    /**
     * Mostra todos os dados de cada posicao da grade no console
     *
     * @since Atualizacao 01
     * @param nome - Nome do usuario a ser carregado
     */
    public void listarGrade( String nome ){
        List<Dados> lista = db.getAllToDosByTag(nome);
        Dados s = lista.get(0);
        String[] x = s.getNote().split(",");

        for(int i = 0; i<x.length; i++) {
            System.out.println("<> POS ["+i+"]"+Integer.parseInt((x[i].replaceAll("[\\D]",""))));
        }
    }

    /**
     * Limpa toda a grade
     *
     * @since Atualizacao 01
     */
    public void limparArrays( ){
        for (int i = 0; i < 240; i++) {
            if (i == 0) {
                cabP[i]  = R.drawable.cab3;
                bcf1P[i] = -1;
                dilP[i]  = -1;
                bcf2P[i] = -1;
                contP[i] = -1;
            } else if (i == 1) {
                cabP[i]  = R.drawable.dil;
                bcf1P[i] = -1;
                dilP[i]  = -1;
                bcf2P[i] = -1;
                contP[i] = -1;
            } else if (i == 2) {
                cabP[i]  = R.drawable.bcf1;
                bcf1P[i] = -1;
                dilP[i]  = -1;
                bcf2P[i] = -1;
                contP[i] = -1;
            } else {
                cabP[i]  = -1;
                bcf1P[i] = -1;
                bcf2P[i] = -1;
                dilP[i]  = -1;
                contP[i] = -1;
            }
        }
    }

    /**
     * Procura no BD pela TAG
     *
     * @since Atualizacao 01
     * @param tagName - Nome do usuario
     * @return true - Se existir
     */
    public boolean tagSearch(String tagName) {
        List<Tag> tags;
        tags = db.getAllTags();
        boolean resp = false;
        for (Tag tag : tags) {
            if(tag.getTagName() != null){
                if (tag.getTagName().equalsIgnoreCase(tagName)) {
                    resp = true;
                }
            }
        }
        System.out.println("de ["+tagName+"] feita com sucesso");
        return resp;
    }

    /**
     * Procura no BD pela nome da TAG
     *
     * @since Atualizacao 01
     * @param tagName - Nome do usuario
     * @return true - Se existir
     */
    public Tag tagByName(String tagName) {
        List<Tag> tags;
        tags = db.getAllTags();
        Tag resp = null;
        for (Tag tag : tags) {
            if(tag.getTagName() != null){
                if (tag.getTagName().equalsIgnoreCase(tagName)) {
                    resp = tag;
                }
            }
        }
        System.out.println("<> ["+tagName+"] encontrado no BD");
        return resp;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    protected void onStop() {
        db.closeDB();
        super.onStop();
    }

    /**
     * Limpa a ultima posicao inserida
     *
     * @since Atualizacao 01
     * @param i - Posicao
     */
    public void limparUltimaPosicao( int i ){
        cabP[i]  = -1;
        bcf1P[i] = -1;
        bcf2P[i] = -1;
        dilP[i] = -1;
        contP[i] = -1;
    }

    @Override
    public void onBackPressed() {
        db.closeDB();
        limparArrays();
        limparUltimaPosicao(posic);
        // Chama a Activity de login
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}