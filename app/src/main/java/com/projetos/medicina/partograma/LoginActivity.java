    package com.projetos.medicina.partograma;

    import android.content.Intent;
    import android.content.SharedPreferences;
    import android.support.v7.app.ActionBarActivity;
    import android.os.Bundle;
    import android.view.Menu;
    import android.view.MenuItem;
    import android.view.View;
    import android.widget.Button;
    import android.widget.EditText;
    import android.widget.Toast;

    public class LoginActivity extends ActionBarActivity {

        Button pesq;
        Button conf;
        EditText login;
        String dados = "";

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_login);
            // Inicializa variaveis
            pesq = (Button) findViewById(R.id.bPesq);
            conf = (Button) findViewById(R.id.bConfirmar);
            login = (EditText) findViewById(R.id.txLogin);
            // Cria o click e passa as variaveis
            conf.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SharedPreferences sharedPreferences = getSharedPreferences("login", MODE_PRIVATE);
                    dados = login.getText().toString();
                    if(dados.equals("") || dados.equals(null) || dados == null){
                        Toast.makeText(getApplicationContext(), "Digite um nome!", Toast.LENGTH_SHORT).show();
                    } else {
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.putExtra("login", dados);
                        intent.putExtra("status", true);
                        sharedPreferences.edit().putBoolean("primeiraExec", true).commit();
                        System.out.println("<> Nome do EditText [" + dados + "]");
                        startActivity(intent);
                    }
                }
            });

        }

        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.menu_login, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.
            int id = item.getItemId();

            //noinspection SimplifiableIfStatement
            if (id == R.id.action_settings) {
                return true;
            }

            return super.onOptionsItemSelected(item);
        }
    }
