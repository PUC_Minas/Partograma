    package com.projetos.medicina.partograma;
    /**
     * @author Paulo Victor de Olveira Leal
     *
     * Atualizacao 01 - Criacao do tratamento para diferentes layouts
     */

    // Dependencias
    import android.app.Activity;
    import android.content.Context;
    import android.content.Intent;
    import android.content.SharedPreferences;
    import android.view.LayoutInflater;
    import android.view.View;
    import android.view.View.OnClickListener;
    import android.view.ViewGroup;
    import android.widget.BaseAdapter;
    import android.widget.ImageView;
    import android.widget.TextView;

    /**
     * Classe que constroi o objeto a ser inserido em cada posicao
     * @version 1.1
     */
    public class CustomAdapter extends BaseAdapter{
        // Variaveis globais
        String [] result;
        Context context;
        int [] cabPos;   // Posicao da cabeca
        int [] dilPos;   // Dilatacao
        int [] bcf1Pos;  // 1ª Hora
        int [] bcf2Pos;  // 2ª Hora
        int [] contPos;  // Contracao

        private static LayoutInflater inflater = null;

        /**
         * Construtor nao-nulo da classe
         *
         * @param mainActivity  - Recebe o MainActivity
         * @param prgmNameList  - Recebe os dados de cada posicao do TextView
         * @param cabP          - Recebe os dados de cada posicao da Posicao da Cabeca
         * @param bcf1P         - Recebe os dados de cada posicao do Batimento Cardiaco 1
         * @param bcf2P         - Recebe os dados de cada posicao do Batimento Cardiaco 2
         * @param dilP          - Recebe os dados de cada posicao da Dilatacao
         * @param contP         - Recebe os dados de cada posicao da Contracao
         */
        public CustomAdapter( MainActivity mainActivity, String[] prgmNameList, int[] cabP,
                              int[] bcf1P, int[] bcf2P, int[] dilP, int [] contP ) {
            result   = prgmNameList;
            context  = mainActivity;
            cabPos   = cabP;
            bcf1Pos  = bcf1P;
            bcf2Pos  = bcf2P;
            contPos  = contP;
            dilPos   = dilP;
            inflater = ( LayoutInflater )context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return result.length;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        /**
         * Classe que cria os objetos de layout
         */
        public class Holder
        {
            TextView tv;       // Texto
            ImageView imgCab;  // Posicao da cabeca
            ImageView imgCon;  // 1ª Hora
            ImageView imgDil;  // 2ª Hora
            ImageView imgBCF1; // Contracao
        }

        /**
         * Metodo gerado automaticamente
         * Pega os objetos do View da posicao da grade
         * @param position      - Posicao atual
         * @param convertView   - View Convertido
         * @param parent        - View Parente
         * @return RowView
         */
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            //Cria o objeto para layout
            Holder holder = new Holder();
            final View rowView;
            // Inicialiaza cada objeto do layout
            if (position >= 0 && position <= 2) {
                rowView = inflater.inflate(R.layout.programlist3, null);
                holder.imgDil = (ImageView) rowView.findViewById(R.id.imageView3);
                holder.tv   = (TextView)  rowView.findViewById(R.id.textView1 );    // TextView
                // Coloca nos TextView os dados de cada posicao
                holder.tv.setText(result[position]);
                // Coloca no ImageView da Dilatacao a imagem de cada posicao

                    dilPos[0] = R.drawable.cab3;
                    dilPos[1] = R.drawable.dil;
                    dilPos[2] = R.drawable.bcf1;
                    holder.imgDil.setImageResource(dilPos[position]);

            }else if (position <= 99 && position >= 21) {
                rowView = inflater.inflate(R.layout.programlist2, null);

                holder.imgDil = (ImageView) rowView.findViewById(R.id.imageView3);  // Dilatacao
                holder.imgBCF1 = (ImageView) rowView.findViewById(R.id.imageView4); // BCF1
                holder.tv   = (TextView)  rowView.findViewById(R.id.textView1 );    // TextView

                // Coloca no ImageView da Dilatacao a imagem de cada posicao
                if( dilPos[position] == -1 ){
                    holder.imgDil.setVisibility(rowView.INVISIBLE);
                }else{
                    holder.imgDil.setImageResource(dilPos[position]);
                }
                // Coloca no ImageView de Batimento Cardiaco 1 a imagem de cada posicao
                if( bcf1Pos[position] == -1 ){
                    holder.imgBCF1.setVisibility(rowView.INVISIBLE);
                }else{
                    holder.imgBCF1.setImageResource(bcf1Pos[position]);
                }
                // Coloca nos TextView os dados de cada posicao
                holder.tv.setText(result[position]);

            } else{
                rowView = inflater.inflate(R.layout.programlist, null);

                holder.tv   = (TextView)  rowView.findViewById(R.id.textView1 );    // TextView
                holder.imgCab = (ImageView) rowView.findViewById(R.id.imageView2);  // Cabeca
                holder.imgCon = (ImageView) rowView.findViewById(R.id.imageView1);  // Contracao
                holder.imgBCF1 = (ImageView) rowView.findViewById(R.id.imageView4); // BCF1

                // Coloca no ImageView de Posicao da Cabeca a imagem de cada posicao
                if(cabPos[position] == -1) {
                    holder.imgCab.setVisibility(rowView.INVISIBLE);
                }else {
                    holder.imgCab.setImageResource(cabPos[position]);
                }
                // Coloca no ImageView da Contracao a imagem de cada posicao
                if( contPos[position] == -1 ){
                    holder.imgCon.setVisibility(rowView.INVISIBLE);
                }else{
                    holder.imgCon.setImageResource(contPos[position]);
                }

                // Coloca no ImageView de Batimento Cardiaco 1 a imagem de cada posicao
                if( bcf1Pos[position] == -1 ){
                    holder.imgBCF1.setVisibility(rowView.INVISIBLE);
                }else{
                    holder.imgBCF1.setImageResource(bcf1Pos[position]);
                }

                // Coloca nos TextView os dados de cada posicao
                holder.tv.setText(result[position]);
            }

            // Cria o tratamento para impedir de clicar nas posicoes padroes
            if( !(position >= 0   && position <= 19  || position >= 20  && position <= 22  ||
                  position >= 40  && position <= 42  || position >= 60  && position <= 62  ||
                  position >= 80  && position <= 82  || position >= 100 && position <= 102 ||
                  position >= 120 && position <= 122 || position >= 140 && position <= 142 ||
                  position >= 160 && position <= 162 || position >= 180 && position <= 182 ||
                  position >= 200 && position <= 202 || position >= 210 && position <= 212 ) ) {
                // Cria o click na posicao
                rowView.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SharedPreferences sp = context.getSharedPreferences("partograma", Context.MODE_PRIVATE);
                        // Cria uma nova intent para iniciar as Opcoes
                        Intent intent = new Intent(context, Options.class);
                        // Limpa todos os dados recebidos no ultimo click
                        sp.edit().putBoolean("first",  false).commit();
                        sp.edit().putBoolean("second", false).commit();
                        sp.edit().putBoolean("dila",    false).commit();
                        sp.edit().putInt("cont",  0).commit();
                        sp.edit().putInt("cab",        0).commit();

                        // Inicia a classe Options
                        startCommentActivity(intent);

                        // Salva a posicao clicada para ser usada nas outras classes
                        sp.edit().putInt("position", position).commit();
                    }
                });
            }
            return rowView;
        }

        /**
         * Metodo para chamar uma nova Activity (no caso chamara a Options)
         * @since 18-07
         * @param i - Intent com as informacoes da classe a ser chamada
         * @return true - Confirmando que iniciou a Activity
         */
        public boolean startCommentActivity(Intent i) {
            // Converte o context para Activity para chamar a nova Activity
            ((Activity) context).startActivityForResult(i, 1);
            return true;
        }
    }