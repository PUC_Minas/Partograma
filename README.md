# Partograma

**Progresso:** CANCELADO<br />
**Autor:** Paulo Victor de Oliveira Leal<br />
**Data:** 2015<br />

### Objetivo
Contruir aplicativo de partograma com o objetivo de substituir os papeis para controle de parto dos medicos, facilitando o controle da gestação e aumentando a possibilidade de um parto sem problemas.
Desenvolvido durante a bolsa de pesquisa, na Faculdade de Medicina da UFMG.

### Observação

IDE:  [Android Studio](https://developer.android.com/studio/)<br />
Linguagem: [JAVA](https://www.java.com/)<br />
Banco de dados: Não utiliza<br />

### Execução

No Android Studio
    
### Contribuição

Esse projeto não está concluído, mas não haverá continuação. É livre para uso.

## Licença
<!---

[//]: <> (The Laravel framework is open-sourced software licensed under the [MIT license]https://opensource.org/licenses/MIT)

-->